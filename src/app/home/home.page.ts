import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    fbid: any;
    fbgender: any;
    fbbirthday: any;
    fbname: any;
    fbemail: any;
    fbimage: any;
    fb_token: any;
    fb_id: any;
    displayName: any;
    email: any;
    familyName: any;
    givenName: any;
    userId: any;
    imageUrl: any;
    sucess: any;
    login: any;
    value: any;
    useremail: any;
    userpassword: any;
    constructor(private http: HttpClient, private router: Router, public navCtrl: NavController, public fb: Facebook, private googlePlus: GooglePlus) {}
    
    loginForm() {
      this.login= {
        email: this.useremail,
        pwd: this.userpassword
      }
      this.http.get('https://d3f90c9a.ngrok.io/posts/'+this.login.email+'/'+this.login.pwd, this.login).subscribe(data => {
        this.value= data;
        console.log(this.value.success);
        if(this.value.success == true) {
          this.router.navigateByUrl('/home');
          console.log(this.login);
        }
      }, err => {
        this.value= err;
        console.log(err);
      });
    }

    loginAction() {
      // Login with permissions
        this.fb.login(['public_profile', 'user_photos', 'email', 'user_birthday'])
        .then( (res: FacebookLoginResponse) => {

          // The connection was successful
            if(res.status == "connected") {

              // Get user ID and Token
                this.fb_id = res.authResponse.userID;
                this.fb_token = res.authResponse.accessToken;

                // Get user infos from the API
                this.fb.api("/me?fields=name,gender,birthday,email,photos", []).then((user) => {
                    // Get the connected user details
                    this.fbid= user.id;
                    this.fbgender= user.gender;
                    this.fbbirthday= user.birthday;
                    this.fbname= user.name;
                    this.fbemail= user.email;
                    this.fbimage= user.photos;
                    
                });
            } 
            else {

              console.log("An error occurred...");
            }
        })
        .catch((e) => {
            console.log('Error logging into Facebook', e);
        });
    }

  googleLogin() {
    this.googlePlus.login({})
    .then(res => {
        console.log(res);
        this.displayName = res.displayName;
        this.email = res.email;
        this.familyName = res.familyName;
        this.givenName = res.givenName;
        this.userId = res.userId;
        this.imageUrl = res.imageUrl;
        this.sucess = "i am working";
    })
    .catch(err => {
        console.error(err);
        this.sucess = "i am not working";
    });
  }
}
